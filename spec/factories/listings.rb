FactoryGirl.define do
  factory :listing do
  	user 
		brand
		category
    title { Faker::Lorem.word }
    description { Faker::Lorem.paragraph}
    price { Faker::Number.number(10) }
    completed { false }
  end
end
