include ActionDispatch::TestProcess

FactoryGirl.define do
  factory :listing_image do
  	listing
  	image { fixture_file_upload(Rails.root.join('spec', 'photos', 'test.png'), 'image/png') }
  end
end
