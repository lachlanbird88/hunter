FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
		name { Faker::Zelda.character }
    password { "password" }
    password_confirmation { "password" }
  end
end
