FactoryGirl.define do
  factory :brand do
			name { Faker::Company.name }
			description { Faker::Company.catch_phrase }
			url { 'https://google.com' }
  end
end
