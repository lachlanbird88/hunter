require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many(:listings).dependent(:destroy) }
  it { should have_many(:offers) }
  it { should have_many(:wishlists) }

	it { should validate_presence_of(:name) }
	it { should validate_presence_of(:email) }
	it { should validate_presence_of(:password) }
end
