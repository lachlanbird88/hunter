require 'rails_helper'

RSpec.describe Listing, type: :model do
  
  it { should belong_to(:user) }
  it { should have_many(:listing_images) }
	it { should belong_to(:brand) }
	it { should belong_to(:category) }
  it { should have_many(:offers) }
  it { should have_many(:wishlists) }

  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:price) }
end

