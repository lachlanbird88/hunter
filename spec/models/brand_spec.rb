require 'rails_helper'

RSpec.describe Brand, type: :model do

		it { should have_many(:listings) }

		it { should validate_presence_of(:name) }
		it { should validate_presence_of(:description) }
		it { should validate_presence_of(:url) }
end
