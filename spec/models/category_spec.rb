require 'rails_helper'

RSpec.describe Category, type: :model do
		it { should have_many(:listings) }
		it { should belong_to(:gender) }
end
