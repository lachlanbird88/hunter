require 'rails_helper'

RSpec.describe ListingImage, type: :model do
  
  let(:listing) { FactoryGirl.create(:listing) }
  before {
		@listing_image = listing.listing_images.build(attributes_for(:listing_image))  	
  }
  it { should belong_to(:listing) }

  it { should have_attached_file(:image) }
  it { should validate_attachment_presence(:image) }
  it { should validate_attachment_content_type(:image).
                allowing('image/png', 'image/gif', 'image/jpg', 'image/jpeg').
                rejecting('text/plain', 'text/xml') }
  it { should validate_attachment_size(:image).
                less_than(2.megabytes) }
end
