ListingImage.destroy_all
Offer.destroy_all
Listing.destroy_all
Brand.destroy_all
User.destroy_all
Category.destroy_all
Gender.destroy_all

Gender.create!(
	name: "male"
)

Gender.create!(
	name: "female"
)

# create 100 users
100.times do |index|
	user = User.new(
		email: Faker::Internet.email,
		name: Faker::Zelda.character,
		password: "password",
		password_confirmation: "password"
	)
  user.skip_confirmation!
  user.save!
end

# create 10 categories
10.times do |index|
	Category.create!(
		gender: Gender.offset(rand(2)).first,
		name: Faker::Commerce.department
	)
end

# create 200 brands
200.times do
	Brand.create!(
    name: Faker::Company.name,
    description: Faker::Company.catch_phrase,
    url: 'https://google.com'
	)
end

# create 1000 listings assigned to random users
1000.times do 
	Listing.create!(
		user: User.offset(rand(100)).first,
		brand: Brand.offset(rand(100)).first,
		category: Category.offset(rand(10)).first,
    title: Faker::Commerce.product_name,
		description: Faker::Lorem.paragraph,
    price: Faker::Commerce.price,
    completed: false
	)
end

Listing.all.each do |listing|
  3.times do
    Offer.create!(
      user: User.offset(rand(100)).first,
      listing: listing,
      price: listing.price,
      stage: Faker::Number.between(0, 3)
    )
  end
end

#1000.times do |listing|
	#ListingImage.create!(
		#listing: Listing.offset(rand(1000)).first,
		#image: Faker::LoremPixel.image
	#)
#end	
