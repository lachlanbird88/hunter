class AddGenderToCategories < ActiveRecord::Migration[5.1]
  def change
    add_reference :categories, :gender, foreign_key: true
  end
end
