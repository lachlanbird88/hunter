class CreateListingImages < ActiveRecord::Migration[5.1]
  def change
    create_table :listing_images do |t|
      t.references :listing, foreign_key: true
      t.attachment :image

      t.timestamps
    end
  end
end
