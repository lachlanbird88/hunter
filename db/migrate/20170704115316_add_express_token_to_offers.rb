class AddExpressTokenToOffers < ActiveRecord::Migration[5.1]
  def change
    add_column :offers, :express_token, :string
  end
end
