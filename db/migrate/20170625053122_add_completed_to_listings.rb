class AddCompletedToListings < ActiveRecord::Migration[5.1]
  def change
    add_column :listings, :completed, :bool
  end
end
