class AddPayerIdToOffers < ActiveRecord::Migration[5.1]
  def change
    add_column :offers, :express_payer_id, :string
    add_column :offers, :ip, :string
  end
end
