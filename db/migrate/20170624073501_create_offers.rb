class CreateOffers < ActiveRecord::Migration[5.1]
  def change
    create_table :offers do |t|
      t.references :listing, foreign_key: true
      t.references :user, foreign_key: true
      t.decimal :price
      t.integer :stage

      t.timestamps
    end
  end
end
