Rails.application.routes.draw do

  resources :offers do
    member do
      get 'accept'
      get 'decline'
      get 'payment'
      get 'express_checkout'
    end
  end
  resources :login, only: [:index]
  resources :search, only: [:index]
	resources :listings do
    member do
      get 'offer'
      post 'favourite'
      post 'unfavourite'
    end
  end

  get '/brands/featured', to: 'brands#featured'
	resources :brands
  resources :category

	devise_for :users, :controllers => { registrations: 'registrations', :omniauth_callbacks => 
    "users/omniauth_callbacks" }
  root to: 'listings#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
