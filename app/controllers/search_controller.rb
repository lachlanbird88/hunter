class SearchController < ApplicationController
  def index
    @q = Listing.ransack(params[:q])
    @listing = @q.result(distinct: true)
  end
end
