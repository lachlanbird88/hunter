class ListingsController < ApplicationController
  before_action :set_listing, only: [:show, :edit, :update, :destroy, :offer, :favourite, :unfavourite]
	before_action :set_dependents
  before_action :set_wishlist, only: [:index, :show]
  before_action :authenticate_user!, :except => [:show, :index]
  before_action :set_listing_information, only: [:index]
  skip_before_action :verify_authenticity_token, only: [:favourite, :unfavourite]

  # GET /listings
  # GET /listings.json
  def index

    @brands = Brand.order(:name)
    @categories = Category.order(:name)
  end

  # GET /listings/1
  # GET /listings/1.json
  def show
  end

  # GET /listings/new
  def new
    @listing = Listing.new
  end

  # GET /listings/1/edit
  def edit
  end

  # GET /listings/1/offer
  def offer
    @offer = Offer.new
    @offer.listing = @listing

    respond_to do |format|
      format.html { render :template => "listings/offer" }
    end
  end
  # POST /listings
  # POST /listings.json
  def create
    @listing = Listing.new(listing_params)
    @listing.user = current_user
    @listing.completed = 0

    respond_to do |format|
      if @listing.save
        if params[:images] 
          params[:images].each do |img|
            @listing.listing_images.create(:image => img)
          end
        end
        format.html { redirect_to @listing, notice: 'Listing was successfully created.' }
        format.json { render :show, status: :created, location: @listing }
      else
        format.html { render :new }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /listings/1
  # PATCH/PUT /listings/1.json
  def update
    respond_to do |format|
      if @listing.update(listing_params)
        if params[:images]
          params[:images].each do |img|
            @listing.listing_images.create(:image => img)
          end
        end
        format.html { redirect_to @listing, notice: 'Listing was successfully updated.' }
        format.json { render :show, status: :ok, location: @listing }
      else
        format.html { render :edit }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /listings/1
  # DELETE /listings/1.json
  def destroy
    @listing.destroy
    respond_to do |format|
      format.html { redirect_to listings_url, notice: 'Listing was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # POST /listings/1/favourite
  def favourite
    wishlist = Wishlist.new
    wishlist.listing = @listing
    wishlist.user = current_user

    respond_to do |format|
      if wishlist.save
        format.json { render json: wishlist, status: :ok }
      else
        format.json { render json: "error", status: :unprocessable_entity } 
      end
    end
  end

  # DELETE /listings/1/unfavourite
  def unfavourite
    wishlist = @listing.wishlists.where(:user => current_user).first or not_found
  
    wishlist.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_listing
			@listing = Listing.friendly.find(params[:id])
    end

		# get all brands and categories when creating a new listing
		def set_dependents
			@brands = Brand.all.order('name')
			@categories = Category.all.order('name')
		end
    
    def set_listing_information
      @maximum_price = Listing.maximum(:price)
      @minimum_price = Listing.minimum(:price)
      @q = Listing.ransack(params[:q])
      @listings = @q.result.filter(params.slice(:brand, :category, :colour, :minPrice, :maxPrice))
        .order(created_at: :desc)
        .paginate(:page => params[:page], :per_page => 32)
    end

    def set_wishlist
      if user_signed_in?
        @wishlists = current_user.wishlists.pluck(:listing_id)
      else
        @wishlists = []
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def listing_params
      params.require(:listing).permit(:category_id, :brand_id, :title, :description, :price, :colour_list)
    end
end
