class CategoryController < ApplicationController
  before_action :set_category, only: [:show]
  before_action :set_wishlist, only: [:show] 
  before_action :set_listing_information, only: [:show]

  # GET /brands
  # GET /brands.json
  def index
    @categories = Category.all
  end

  def show
    @brands = Brand.order(:name)
  end

  private

  def set_category
    @category = Category.friendly.find(params[:id])
  end

  def set_listing_information
    @maximum_price = Listing.maximum(:price)
    @minimum_price = Listing.minimum(:price)

    @listings = @category.listings.filter(params.slice(:brand, :colour))
      .order(created_at: :desc)
      .paginate(:page => params[:page], :per_page => 32)
  end

  def set_wishlist
    if user_signed_in?
      @wishlists = current_user.wishlists.pluck(:listing_id)
    else
      @wishlists = []
    end
  end
end

