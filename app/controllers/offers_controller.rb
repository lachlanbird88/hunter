class OffersController < ApplicationController
  before_action :set_offer, only: [:show, :edit, :update, :destroy, :accept, :decline, :payment, :express_checkout]
  before_action :authenticate_user!

  # GET /offers
  # GET /offers.json
  def index
    @active_offers = current_user.offers.where("stage NOT IN (0, 3)")
    @completed_offers = current_user.offers.where("stage IN (0, 3)")

    @active_listings = Offer.joins(:listing).where("listings.user_id" => current_user.id)
      .where("listings.completed = false AND offers.stage NOT IN (0, 3)")
    @completed_listings = Offer.joins(:listing).where("listings.user_id" => current_user.id)
      .where("listings.completed = true and offers.stage = 3") 

    @wishlists = Listing.joins(:wishlists).where("wishlists.user_id" => current_user.id)
  end

  # GET /offers/1
  # GET /offers/1.json
  def show
  end

  # GET /offers/new
  def new
    @offer = Offer.new
  end

  # GET /offers/1/edit
  def edit
  end

  # POST /offers
  # POST /offers.json
  def create
    @offer = Offer.new(offer_params)
    @offer.user_id = current_user.id
    @offer.stage = 1

    respond_to do |format|
      if @offer.save
				format.html { redirect_to @offer.listing, notice: 'Please wait for an email from the seller to see whether you were successful!' }
        format.json { render :show, status: :created, location: @offer }
      else
        format.html { render :new }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offers/1
  # PATCH/PUT /offers/1.json
  def update
    respond_to do |format|
      if @offer.update(offer_params)
        format.html { redirect_to @offer, notice: 'Offer was successfully updated.' }
        format.json { render :show, status: :ok, location: @offer }
      else
        format.html { render :edit }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offers/1
  # DELETE /offers/1.json
  def destroy
    @offer.destroy
    respond_to do |format|
      format.html { redirect_to offers_url, notice: 'Offer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /offers/1/accept
  def accept
    @listing = @offer.listing
    unless @listing.user_id == current_user.id
      format.html { redirect_to @offer, alert: 'You do not have permission to accept this offer!' } 
    end

    @offer.stage = 2
    
    respond_to do |format|
      if @offer.save
        format.html { redirect_to offers_url, notice: 'Offer was accepted!.' }
        format.json { render :show, status: :created, location: @offer }
      else
        format.html { render :index }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /offers/1/decline
  def decline
    @listing = @offer.listing
    unless @listing.user_id == current_user.id
      format.html { redirect_to @offer, alert: 'You do not have permission to decline this offer!' } 
    end

    @offer.stage = 0
    
    respond_to do |format|
      if @offer.save
        format.html { redirect_to offers_url, notice: 'Offer was declined!.' }
        format.json { render :show, status: :created, location: @offer }
      else
        format.html { render :index }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  def express_checkout
    response = EXPRESS_GATEWAY.setup_purchase(@offer.price*100,
      ip: request.remote_ip,
      return_url: payment_offer_url(@offer),
      cancel_return_url: offers_url,
      currency: "AUD",
      allow_guest_checkout: false,
      items: [{name: @offer.listing.title, description: @offer.listing.description, quantity: "1", amount: @offer.price*100}]
    )
    redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
  end
  
  # GET /offers/1/payment
  def payment
    @listing = @offer.listing

    unless @offer.user_id == current_user.id
      format.html { redirect_to @offer, alert: 'To do not have permission to make payment for this listing!' }
    end

    # TODO: do paypal stuff here
    @offer.stage = 3
    @listing.completed = 1 
    @listing.save
    @offer.ip = request.remote_ip
    @offer.express_token = params[:token]
    if @offer.save
      if @offer.purchase  
        redirect_to offers_url @offer
      end
    else 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offer
      @offer = Offer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offer_params
      params.require(:offer).permit(:listing_id, :price)
    end
end
