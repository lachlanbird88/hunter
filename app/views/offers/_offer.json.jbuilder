json.extract! offer, :id, :listing_id, :user_id, :price, :stage, :created_at, :updated_at
json.url offer_url(offer, format: :json)
