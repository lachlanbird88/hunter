/*
 * M-Store | Modern E-Commerce Template
 * Copyright 2016 rokaux
 * Theme Custom Scripts
 */

jQuery(document).ready(function($) {
	'use strict';

	

	// Animated Scroll to Top Button
	//------------------------------------------------------------------------------
	var $scrollTop = $('.scroll-to-top-btn');
	if ($scrollTop.length > 0) {
		$(window).on('scroll', function(){
	    if ($(window).scrollTop() > 600) {
	      $scrollTop.addClass('visible');
	    } else {
	      $scrollTop.removeClass('visible');
	    }
		});
		$scrollTop.on('click', function(e){
			e.preventDefault();
			$('html').velocity("scroll", { offset: 0, duration: 1000, easing:'easeOutExpo', mobileHA: false });
		});
	}


	// Smooth scroll to element
	//------------------------------------------------------------------------------
	var $scrollTo = $('.scroll-to');
	$scrollTo.on('click', function(event) {
		var $elemOffsetTop = $(this).data('offset-top');
		$('html').velocity("scroll", { offset:$(this.hash).offset().top-$elemOffsetTop, duration: 1000, easing:'easeOutExpo', mobileHA: false});
		event.preventDefault();
	});


	// Toolbar Dropdown
	//------------------------------------------------------------------------------
	var toolbarToggle = $('.toolbar-toggle'),
			toolbarDropdown = $('.toolbar-dropdown'),
			toolbarSection = $('.toolbar-section');

	function closeToolBox() {
		toolbarToggle.removeClass('active');
		toolbarSection.removeClass('current');
	}

	toolbarToggle.on('click', function(e) {
		var currentValue = $(this).attr('href');
		if($(e.target).is('.active')) {
			closeToolBox();
			toolbarDropdown.removeClass('open');
		} else {
			toolbarDropdown.addClass('open');

			closeToolBox();
			$(this).addClass('active');
			$(currentValue).addClass('current');
		}
		e.preventDefault();
	});
	$('.close-dropdown').on('click', function() {
		toolbarDropdown.removeClass('open');
		toolbarToggle.removeClass('active');
		toolbarSection.removeClass('current');
	});

	var toggleSection = $('.toggle-section');

	toggleSection.on('click', function(e) {
		var currentValue = $(this).attr('href');
		toolbarSection.removeClass('current');
		$(currentValue).addClass('current');
		e.preventDefault();
	});


	// Toggle Submenu
	//------------------------------------------------------------------------------
	var $hasSubmenu = $('.menu-item-has-children > a');

	function closeSubmenu() {
		$hasSubmenu.parent().removeClass('active');
	}
	$hasSubmenu.on('click', function(e) {
		if($(e.target).parent().is('.active')) {
			closeSubmenu();
		} else {
			closeSubmenu();
			$(this).parent().addClass('active');
		}
	});


	// Shop Filters Toggle
	//------------------------------------------------------------------------------
	var filtersToggle = $('[data-toggle="filters"]'),
			filtersWrap = $('.filters'),
			filtersPane = $('.filters-pane');
	function closeFilterPane() {
		filtersToggle.removeClass('active');
		filtersPane.removeClass('open');
		filtersWrap.css('height', 0);
	}
	filtersToggle.on('click', function(e) {
		var currentFilter = $(this).attr('href');
		if($(this).is('.active')) {
			closeFilterPane();
		} else {
			closeFilterPane();
			$(this).addClass('active');
			filtersWrap.css('height', $(currentFilter).outerHeight());
			$(currentFilter).addClass('open');
		}
		e.preventDefault();
	});
	if(typeof window.Modernizr !== "undefined" && !Modernizr.touch) {
		$(window).on('resize', function() {
			closeFilterPane();
		});
	}

	// Sidebar Toggle on Mobile
	//------------------------------------------------------------------------------
	var sidebar = $('.sidebar'),
			sidebarToggle = $('.sidebar-toggle');
	sidebarToggle.on('click', function() {
		$(this).addClass('sidebar-open');
		sidebar.addClass('open');
	});
	$('.sidebar-close').on('click', function() {
		sidebarToggle.removeClass('sidebar-open');
		sidebar.removeClass('open');
	});

  // Range Slider
	//------------------------------------------------------------------------------
	var rangeSlider  = document.querySelector('.ui-range-slider');
	if(typeof rangeSlider !== 'undefined' && rangeSlider !== null) {
		var dataStartMin = parseInt(rangeSlider.parentNode.getAttribute( 'data-start-min' ), 10),
				dataStartMax = parseInt(rangeSlider.parentNode.getAttribute( 'data-start-max' ), 10),
				dataMin 		 = parseInt(rangeSlider.parentNode.getAttribute( 'data-min' ), 10),
				dataMax   	 = parseInt(rangeSlider.parentNode.getAttribute( 'data-max' ), 10),
				dataStep  	 = parseInt(rangeSlider.parentNode.getAttribute( 'data-step' ), 10);
		var valueMin 			= document.querySelector('.ui-range-value-min span'),
				valueMax 			= document.querySelector('.ui-range-value-max span'),
				valueMinInput = document.querySelector('.ui-range-value-min input'),
				valueMaxInput = document.querySelector('.ui-range-value-max input');
		noUiSlider.create(rangeSlider, {
			start: [ dataStartMin, dataStartMax ],
			connect: true,
			step: dataStep,
			range: {
				'min': dataMin,
				'max': dataMax
			}
		});
		rangeSlider.noUiSlider.on('update', function(values, handle) {
			var value = values[handle];
			if ( handle ) {
				valueMax.innerHTML  = Math.round(value);
				valueMaxInput.value = Math.round(value);
			} else {
				valueMin.innerHTML  = Math.round(value);
				valueMinInput.value = Math.round(value);
			}
		});

	}
  
  // Product Gallery
	//------------------------------------------------------------------------------
	var galleryThumb = $('.product-gallery-thumblist a'),
			galleryPreview = $('.product-gallery-preview > li');

	// Thumbnails
	//------------------------------------------------------------------------------
	galleryThumb.on('click', function(e) {
		var target = $(this).attr('href');

		galleryThumb.parent().removeClass('active');
		$(this).parent().addClass('active');
		galleryPreview.removeClass('current');
		$(target).addClass('current');

		e.preventDefault();
	});

  // Waves Effect (on Buttons)
	//------------------------------------------------------------------------------
	if($('.waves-effect').length) {
		Waves.displayEffect( { duration: 600 } );
	}

  var applyFiltersButton = $('.apply-filters');

  applyFiltersButton.on('click', function(e) {
    var selectedCategories = $('[data-type="checkbox-category"]:checked').map(function() {
      return this.value;  
    }).get().join('-');

    var selectedBrands = $('[data-type="checkbox-brand"]:checked').map(function() {
      return this.value;  
    }).get().join('-');

    if(typeof rangeSlider != 'undefined') {
      var minPrice = rangeSlider.noUiSlider.get()[0];
      var maxPrice = rangeSlider.noUiSlider.get()[1];
    }

    var query = {
      "category": selectedCategories, 
      "brand": selectedBrands,
      "minPrice": minPrice,
      "maxPrice": maxPrice
    };

    window.location.href = getBaseUrl() + '?' + encodeQueryData(query);
  });

 	var categories = $('[data-type="checkbox-category"]');
 	var brands = $('[data-type="checkbox-brand"]');
 	
 	window.addEventListener('load', function(e) {
	 	var selectedCategories = getQueryVariable("category").split("-");
	 	var selectedBrands = getQueryVariable("brand").split("-");

	 	$(categories).filter(function(){
			return selectedCategories.indexOf($(this).val()) > -1;
		}).prop("checked", true);

	 	$(brands).filter(function(){
			return selectedBrands.indexOf($(this).val()) > -1;
		}).prop("checked", true);

	 	if(typeof rangeSlider != 'undefined') {
			var minPrice = getQueryVariable("minPrice");
		 	var maxPrice = getQueryVariable("maxPrice");

		 	rangeSlider.noUiSlider.set([minPrice, maxPrice]);
		}
	});

  function getBaseUrl() {
    return window.location.protocol + '//' + window.location.host + window.location.pathname;
  }

  function encodeQueryData(data) {
    var ret = [];
    for (var d in data) {
      ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    }
    return ret.join('&');
  }

  function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
  }

  var addToWishlistButton = $('.add-to-whishlist');

  addToWishlistButton.on('click', function() {
    var that = this;
    var data = $(this).data();
    var id = data.id; 
    var favourite = data.favourite;
    var alreadyFavourited = !!favourite;

    $.ajax({
      type: "POST", 
      url: "/listings/" + id + "/" + (alreadyFavourited ? "unfavourite" : "favourite"),
      dataType: "json",
      success: function() {
        if(alreadyFavourited) {
          $(that).css('background-color', 'white');
          $(that).css('color', '#999');
          data.favourite = false;
        }
        else {
          $(that).css('background-color', 'red');
          $(that).css('color', 'white');
          data.favourite = true;
        }
      },
      error: function() {
        // show error
      }
    });
  });


  // Toggle Mobile Menu
	//------------------------------------------------------------------------------
	var menuToggle = $('.mobile-menu-toggle'),
			mobileMenu = $('.main-navigation');
	menuToggle.on('click', function() {
		$(this).toggleClass('active');
		mobileMenu.toggleClass('open');
	});

});/*Document Ready End*/
