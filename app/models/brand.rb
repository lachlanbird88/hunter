class Brand < ApplicationRecord

		# setup friendly id
		extend FriendlyId
		friendly_id :name, use: :slugged

		has_many :listings

		validates_presence_of :name, :description, :url, :slug
end
