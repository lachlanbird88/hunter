class Listing < ApplicationRecord

  # allow filterable trait
  include Filterable

	# setup friendly id
	extend FriendlyId
	friendly_id :slug_candidates, use: :slugged

  belongs_to :user
	belongs_to :brand
	belongs_to :category

  has_many :listing_images, dependent: :destroy
  has_many :offers
  has_many :wishlists
	
	# taggable
	acts_as_taggable_on :colours

  # validation
  validates_presence_of :title, :description, :price

  # scopes
  scope :category, -> (category) { where category: category.split('-') }
  scope :brand, -> (brand) { where brand: brand.split('-') }
  scope :colour, -> (colour) { joins(:taggings).where(:taggings => { :tag_id => colour.split('-') } ) }
  scope :minPrice, -> (minPrice) { where 'price >= ?', minPrice }
  scope :maxPrice, -> (maxPrice) { where 'price <= ?', maxPrice }

	def slug_candidates
		[
      :title,
      [:id, :title],
      [:id, :title, :user_id]
		]
	end

	def first_image
		self.listing_images.first ? self.listing_images.first.image.url : "https://placeholdit.co//i/315x315"
	end
end
