class ListingImage < ApplicationRecord
  belongs_to :listing

  has_attached_file :image, styles: { thumbnail: "100x100", med: "200x200", large: "400x400" }

  validates_presence_of :image
  validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
  validates_attachment_size :image, :less_than => 2.megabytes
end
