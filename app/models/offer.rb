class Offer < ApplicationRecord
  belongs_to :listing
  belongs_to :user

  validates_presence_of :price

  def print_stage
    case stage
      when 0
        "Offer Declined"
      when 1
        "Pending"
      when 2
        "Awaiting Payment"
      when 3
        "Payment Received"
    end
  end

  def purchase 
    response = EXPRESS_GATEWAY.purchase(price*100, express_purchase_options)
    response.success?
  end

  def stage_class
    case stage
      when 0
        "text-danger"
      when 1
      when 2
        "text-warning"
      when 3
        "text-success"
    end
  end

  def express_token=(token)
    self[:express_token] = token
    if !token.blank?
      # you can dump details var if you need more info from buyer
      details = EXPRESS_GATEWAY.details_for(token)
      self.express_payer_id = details.payer_id
    end
  end

  private

  def express_purchase_options
    {
      :ip => ip,
      :currency => "AUD",
      :token => express_token,
      :payer_id => express_payer_id
    }
  end
end
